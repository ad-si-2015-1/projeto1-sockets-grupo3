
import java.net.*;
import java.io.*;
import java.util.Random;

public class Util {
    
    private static final int numeroDePortas = 10;
    private static final int numeroDeIps = 10;
        
    public static int gerarPorta() {
        Random rand = new Random();
        return (1024 + rand.nextInt(numeroDePortas));
    }
    
    public static String gerarIp() {
        Random rand = new Random();
        //Mudar o ip de acordo com a sub rede que irá executar!!!
        String ip = "192.168.1." + rand.nextInt(numeroDeIps);
        return ip;
        //Trabalhar local host
        //return ip = "localhost";
    }
    
    public static String conseguirIp(String consulta) {
        String ip;
        String endereco[] = consulta.split(":");
        ip = endereco[0];
        return ip;
    } 
    
    public static int conseguirPorta(String consulta) {
        int porta;
        String endereco[] = consulta.split(":");
        porta = Integer.parseInt(endereco[1]);
        return porta;
    }
    
    public static String ProcurarAgente() {
        int porta = 1234;
        String servidor = "localhost";
        String resposta;
        String pergunta;
        
        try {    
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(servidor);

            byte[] sendData = new byte[64];
            byte[] receiveData = new byte[64];
            
            pergunta = "Aonde tem um Agente?";
            sendData = pergunta.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, porta);
            System.out.println(pergunta);
            clientSocket.send(sendPacket);
            clientSocket.setSoTimeout(5000);

            try{
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

                clientSocket.receive(receivePacket);

                resposta = new String(receivePacket.getData());
                resposta = resposta.trim();
                clientSocket.close();
                return resposta;

            } catch(SocketTimeoutException e) {
                clientSocket.close();
                return "Nao respondeu";
                //return "192.168.1.3:" + Util.gerarPorta() + ":AgentePassivo";
            }
        } catch (Exception e) {
         return "Nao respondeu";   
        }
    }
    
}
