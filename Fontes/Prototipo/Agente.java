
import java.net.*;
import java.util.Scanner;

public class Agente extends Thread{
    static String status;
    static String tipo;
    private static int porta;
    static final int tempoEntrePerguntas = 1000;
    
    public Agente(){
        porta = Util.gerarPorta();
    }
    
    @Override
    public void run() {
        try {
            DatagramSocket serverSocket = new DatagramSocket(porta);
            byte[] receiveData = new byte[64];
            byte[] sendData = new byte[64];

            while (true) {

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                System.out.println("Aguardando pergunta na porta " + porta);
                
                serverSocket.receive(receivePacket);
                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                
                String pergunta = new String(receivePacket.getData());
                pergunta = pergunta.trim();
                String resposta;
                DatagramPacket sendPacket;
                
                switch(pergunta){
                    default: 
                        resposta = responderQuestionador(pergunta);
                        break;
                    case "Tem um Agente aqui?":
                        System.out.println(pergunta);
                        resposta = responderAranha();
                        break;
                    case "Mordida":
                        resposta = responderZumbi();
                        break;
                    case "Apreensao":
                        resposta = responderCacador();
                        break;
                    case "Cura":
                        resposta = responderCurador();
                        break;
                }
                sendData = resposta.getBytes();
                sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                serverSocket.send(sendPacket);
                       
                }
            
        } catch (Exception e) {
            
        }
    }
    
    private String responderAranha(){
        String resposta;
        resposta = tipo;
        System.out.println("Sim, um " + tipo + "\n\n");
        return resposta;        
    }
    
    private String responderQuestionador(String pergunta) throws Exception{
        int numero1;
        int numero2;
        int resultado;
        String operador;
        
        String partes[] = pergunta.split(";");
        numero1 = Integer.parseInt(partes[0]);
        operador = partes[1];
        numero2 = Integer.parseInt(partes[2]);
        
        switch(operador) {
            default: resultado = numero1 + numero2; break;
            case "-": resultado = numero1 - numero2; break;
            case "*": resultado = numero1 * numero2; break;
        }
        System.out.println(numero1 + " " + operador + " " + numero2 + " = " + resultado);
        return Integer.toString(resultado);
    }
    
    private String responderZumbi() {
        String resposta;
        if(status.equals("saudavel")) {
            status = "infectado";
            resposta = "Aaaaaaaaaaaa";
            System.out.println("Voce virou um zumbi");
        } else {
            resposta = "Sou imune";
            System.out.println("Voce e imune ao zumbi");
        }
        return resposta;
    }
    
    private String responderCacador() {
        String resposta;
        if(status.equals("infectado")) {
            status = "preso";
            resposta = "Brains";
            System.out.println("Voce esta preso");
        } else {
            resposta = "Nao sou zumbi";
            System.out.println("So zumbis sao presos");
        }
        return resposta;
    }
    
    private String responderCurador() {
        String resposta;
        if(status.equals("infectado")) {
            status = "saudavel";
            resposta = "Obrigado";
            System.out.println("Voce esta curado");
        } else {
            resposta = "Nao sou zumbi";
            System.out.println("So zumbis sao presos");
        }
        return resposta;
    }
    
    private static void iniciarAgente(){
        Scanner escolha = new Scanner(System.in);
        System.out.println("Escolha o tipo do Agente: ");
        System.out.println("1 - Questionador/Respondedor\n2 - Zumbi\n3 - Cacador\n"
        + "4 - Curador\n");
        while(null == tipo) {
            switch(escolha.nextInt()){
            case 1:
                tipo = "Questionador/Respondedor"; 
                status = "saudavel";
                break;
            case 2:
                tipo = "Zumbi"; 
                status = "infectado";
                break;
            case 3:
                tipo = "Cacador"; 
                status = "cacando";
                break;
            case 4:
                tipo = "Curador";
                status = "curando";
                break;
            default: System.out.println("Tipo incorreto, escolha novamente");
            }
        }
        
        System.out.println("Tipo: " + tipo + "\n\n");
    }
    
    public static void main(String[] args) throws Exception{
        
        iniciarAgente();
        
        Thread passiva = new Thread(new Agente());
        passiva.start();
        
        Thread ativa;
        
        while(true) {
            String local = Util.ProcurarAgente();
            //String local = "192.168.1.3:" + porta + ":" + tipo;
            if(local.equals("Não tem nenhum!") || local.equals("Nao respondeu")){
                System.out.println(local);
                Thread.sleep(10000);
            } else {
                System.out.println(local);
                String servidor = Util.conseguirIp(local);
                int porta = Util.conseguirPorta(local);
                System.out.println(servidor + ":" + porta);
                
                switch(status){
                    case "saudavel":
                        System.out.println("Perguntando para ele...");
                        ativa = new Thread(new Questionador(servidor, porta));
                        ativa.start();
                        break;
                    case "infectado":
                        System.out.println("Mordendo ele...");
                        ativa = new Thread(new Zumbi(servidor, porta));
                        ativa.start();
                        break;
                    case "cacando":
                        System.out.println("Prendendo ele se for zumbi...");
                        ativa = new Thread(new Cacador(servidor, porta));
                        ativa.start();
                        break;
                    case "curando":
                        System.out.println("Curando ele se for zumbi...");
                        ativa = new Thread(new Curador(servidor, porta));
                        ativa.start();
                        break;
                    case "preso":
                        System.out.println("Voce esta preso...");
                        break;
                    default: break;
                }
            }
            Thread.sleep(tempoEntrePerguntas);
        }
        
        
    }
    
}
