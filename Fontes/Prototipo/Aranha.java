
import java.net.*;
import java.io.*;

public class Aranha extends Thread{
    static int porta;
    static String servidor;
    static Dns dns = new Dns();
    final int timeOut = 2000;
    final int tempoEntroBuscas = 10000;
    
    @Override
    public void run() {
        String resposta;
        String pergunta;
        
        while(true) {
            
            servidor = Util.gerarIp();
            porta = Util.gerarPorta();
           
                try {    
                    BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
                    DatagramSocket clientSocket = new DatagramSocket();

                    InetAddress IPAddress = InetAddress.getByName(servidor);

                    byte[] sendData = new byte[64];
                    byte[] receiveData = new byte[64];

                    pergunta = "Tem um Agente aqui?";
                    System.out.println("Em " + servidor + ":" + porta);
                    System.out.println(pergunta);
                    sendData = pergunta.getBytes();
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, porta);

                    clientSocket.send(sendPacket);
                    clientSocket.setSoTimeout(timeOut);

                    try{
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

                        clientSocket.receive(receivePacket);

                        resposta = new String(receivePacket.getData());
                        resposta = resposta.trim();
                        String agente = 
                                receivePacket.getAddress().getHostName() + ":" 
                                + receivePacket.getPort() + ":" 
                                + resposta;
                        System.out.println("Sim, é o " + agente);
                        dns.setUltimoAgente(agente);
                    } catch(SocketTimeoutException e) {
                        resposta = "Aparentemente nao!"; 
                        System.out.println(resposta);
                    }
                
                clientSocket.close();
            } catch (Exception e) {
                    System.out.println("A busca nao esta funcionando!");
            }
            
            try {
                Thread.sleep(tempoEntroBuscas);
                 System.out.println("\n\n");
            } catch (InterruptedException ex) {
                 System.out.println("\n\n");
            }
                
        }
        
    }
    
    public static void main(String[] args) throws Exception{
     
        Thread ativa = new Thread(new Aranha());
        Thread passiva = new Thread(dns);
        
        passiva.start();
        Thread.sleep(2000);
        ativa.start();
        
    }
    
    
}
