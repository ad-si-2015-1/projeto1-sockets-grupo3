Para rodar, baixe os arquivos que estão dentro do diretório Fontes/Prototipo  

Compile usando o comando javac *.java

Inicialize o agente Aranha
Inicialize a classe Agente
A classe Agente é que inicializa todos os outros.
Ao inicializar a classe Agente, encontrará o seguinte menu.

Escolha o tipo do Agente: 
1. Questionador/Respondedor
2. Zumbi
3. Cacador
4. Curador

Digite os numeros de 1 a 4 para inicializar.