
import java.io.*;
import java.net.*;
import java.util.Random;

public class Questionador implements Runnable{
    
    int porta;
    String servidor;
    
    public Questionador(String servidor, int porta){
        this.servidor = servidor;
        this.porta = porta;
    }
    
    @Override
    public void run() {
        String resposta;
        String pergunta;
        
        try {    
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(servidor);

            byte[] sendData = new byte[64];
            byte[] receiveData = new byte[64];
            
            pergunta = gerarConta();
            sendData = pergunta.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, porta);
            
            clientSocket.send(sendPacket);
            clientSocket.setSoTimeout(5000);

            try{
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

                clientSocket.receive(receivePacket);

                resposta = new String(receivePacket.getData());
                resposta = resposta.trim();
                System.out.println(resposta);

            } catch(SocketTimeoutException e) {
                resposta = "Nao respondeu";            
            }

            clientSocket.close();
        } catch (Exception e) {
            
        }
    }
    
    private static String gerarConta() {
        Random gerador = new Random();
        int numero1 = gerador.nextInt(1000);
        int numero2 = gerador.nextInt(1000);
        String operador;
        switch(gerador.nextInt(3)) {
            default: operador ="+"; break;
            case 1: operador = "-"; break;
            case 2: operador = "*"; break;
        }
        
        String pergunta = numero1 + ";" + operador + ";" + numero2;
        System.out.println(numero1 + " " + operador + " " + numero2);
        return pergunta;
    }
    
    
}
