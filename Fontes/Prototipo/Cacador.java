

import java.net.*;
import java.io.*;

public class Cacador implements Runnable{
    
    int porta;
    String servidor;
    
    public Cacador(String servidor, int porta){
        this.servidor = servidor;
        this.porta = porta;
    }
    
    @Override
    public void run() {
        String resposta;
        String pergunta;
        
        try {    
            BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
            DatagramSocket clientSocket = new DatagramSocket();

            InetAddress IPAddress = InetAddress.getByName(servidor);

            byte[] sendData = new byte[64];
            byte[] receiveData = new byte[64];
            
            pergunta = "Apreensao";
            sendData = pergunta.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, porta);
            
            clientSocket.send(sendPacket);
            clientSocket.setSoTimeout(5000);

            try{
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

                clientSocket.receive(receivePacket);

                resposta = new String(receivePacket.getData());
                resposta = resposta.trim();
                System.out.println(resposta);

            } catch(SocketTimeoutException e) {
                resposta = "Nao deu certo";            
            }

            clientSocket.close();
        } catch (Exception e) {
            
        }
    }
}
