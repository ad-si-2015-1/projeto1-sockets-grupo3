
import java.net.*;

public class Dns extends Thread{
    
    String ultimoAgente;
    int porta = 1234;
    
    public Dns(){
        ultimoAgente = "Não tem nenhum!";
    }
    
    public void setUltimoAgente(String agente) {
        ultimoAgente = agente;
    }
    
    @Override
    public void run() {
        try {
            DatagramSocket serverSocket = new DatagramSocket(porta);
            
            byte[] receiveData = new byte[64];
            byte[] sendData = new byte[64];
            System.out.println("Aguardando pergunta na porta " + porta);
            while(true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                
                serverSocket.receive(receivePacket);
                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();
                
                String pergunta = new String(receivePacket.getData());
                pergunta = pergunta.trim();
                String resposta;
                DatagramPacket sendPacket;
                resposta = ultimoAgente;
                //resposta = "192.168.1.3:" + porta + ":AgentePassivo";
                sendData = resposta.getBytes();
                sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                serverSocket.send(sendPacket);
            }
        } catch(Exception e) {
            
        }
    }
    
}
